package nl.ordina.pieter.wordcount.util;

import nl.ordina.pieter.wordcount.contract.WordFrequency;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WordFrequencyUtilTest {

    @Test
    void createListOfTopNWordFrequencies_should_return_list_of_top_n_wordfrequencies() {
        String text = "one two two three three three four four four four";
        List<WordFrequency> topNWordFrequencies = WordFrequencyUtil.createListOfTopNWordFrequencies(text, 2);
        assertEquals(2, topNWordFrequencies.size());
        assertEquals(topNWordFrequencies.get(0).getWord(), "four");
        assertEquals(topNWordFrequencies.get(0).getFrequency(), 4);
        assertEquals(topNWordFrequencies.get(1).getWord(), "three");
        assertEquals(topNWordFrequencies.get(1).getFrequency(), 3);
    }

    @Test
    void createListOfTopNWordFrequencies_should_return_list_of_top_n_wordfrequencies_lake_example() {
        String text = "The sun shines over the lake.";
        List<WordFrequency> topNWordFrequencies = WordFrequencyUtil.createListOfTopNWordFrequencies(text, 3);
        assertEquals(3, topNWordFrequencies.size());
        assertEquals(topNWordFrequencies.get(0).getWord(), "the");
        assertEquals(topNWordFrequencies.get(0).getFrequency(), 2);
        assertEquals(topNWordFrequencies.get(1).getWord(), "lake");
        assertEquals(topNWordFrequencies.get(1).getFrequency(), 1);
        assertEquals(topNWordFrequencies.get(2).getWord(), "over");
        assertEquals(topNWordFrequencies.get(2).getFrequency(), 1);
    }

    @Test
    void getTopNWordFrequencies_should_return_map_of_n_most_frequent_words() {
        String text = "one two two three three three four four four four";
        Map<String, Integer> map = WordFrequencyUtil.getTopNWordFrequencies(text, 2);
        assertThat(map.keySet(), contains("four", "three"));
    }

    @Test
    void getTopNWordFrequencies_should_return_map_that_is_alphabetically_ordered_when_frequency_is_the_same() {
        String text = "a b c d d";
        Map<String, Integer> map = WordFrequencyUtil.getTopNWordFrequencies(text, 3);
        assertThat(map.keySet(), contains("d", "a", "b"));
    }

    @Test
    void createMapOfWordFrequencies_should_create_map_with_frequencies_of_words_in_text() {
        String text = "one two two three three three four four four four";
        Map<String, Integer> map = WordFrequencyUtil.createMapOfWordFrequencies(text);
        assertEquals(1, map.get("one"));
        assertEquals(2, map.get("two"));
        assertEquals(3, map.get("three"));
        assertEquals(4, map.get("four"));
    }

    @Test
    void createMapOfWordFrequencies_should_ignore_capitalization_when_mapping_frequencies_of_words_in_text_() {
        String text = "one two Two three Three tHREE four FOUR foUR FOur";
        Map<String, Integer> map = WordFrequencyUtil.createMapOfWordFrequencies(text);
        assertEquals(1, map.get("one"));
        assertEquals(2, map.get("two"));
        assertEquals(3, map.get("three"));
        assertEquals(4, map.get("four"));
    }

    @Test
    void createMapOfWordFrequencies_should_return_map_that_is_ordered_alphabetically() {
        String text = "one two Two three Three tHREE four FOUR foUR FOur";
        Map<String, Integer> map = WordFrequencyUtil.createMapOfWordFrequencies(text);
        assertThat(map.keySet(), contains("four", "one", "three", "two"));
    }

    @Test
    void createListOfWords_should_create_list_with_same_number_of_words_as_text() {
        String text = "This is a text with some test words to test whether the method createListOfWords " +
                "creates a list that has the same number of words has this test. Some words in this text " +
                "occur more than once but each occurrence should be added to the list of words that is " +
                "returned by the method createListOfWords. The number of words in this text is 62.";
        List<String> words = WordFrequencyUtil.createListOfWords(text);
        assertEquals(62, words.size());
    }

    @Test
    void createListOfWords_should_create_list_with_same_number_of_words_as_text_with_different_delimiters() {
        String text = "This1is2a3text4with5some6test7words8910to§test!whether@the#method$createListOfWords%" +
                "creates^a&list*that(has)the-_+=same    number][of{}words;has:this|test.Some,./words<>in/this?text " +
                "occur\\more    than\tonce\''but each occurrence should be added to the list of words that is " +
                "returned by the method!!!createListOfWords.        The number of words11in22this33text33is  62.    ";
        List<String> words = WordFrequencyUtil.createListOfWords(text);
        assertEquals(62, words.size());
    }

    @Test
    void createListOfWords_should_create_list_with_all_words_from_text() {
        String text = "This is a text this.";
        List<String> words = WordFrequencyUtil.createListOfWords(text);
        assertTrue(words.contains("this"));
        words.remove("this");
        assertTrue(words.contains("is"));
        words.remove("is");
        assertTrue(words.contains("a"));
        words.remove("a");
        assertTrue(words.contains("text"));
        words.remove("text");
        assertTrue(words.contains("this"));
        words.remove("this");
        assertTrue(words.isEmpty());
    }

    @Test
    void createListOfCharacters_should_create_list_with_same_number_of_characters_as_text() {
        String text = "this is a    !@$%?>    text with 46 characters";
        List<Character> characters = WordFrequencyUtil.createListOfCharacters(text);
        assertEquals(46, characters.size());
    }

    @Test
    void createListOfCharacters_should_create_list_with_all_characters_from_text() {
        String text = "test";
        List<Character> characters = WordFrequencyUtil.createListOfCharacters(text);
        assertTrue(characters.contains('t'));
        characters.remove((Character) 't');
        assertTrue(characters.contains('e'));
        characters.remove((Character) 'e');
        assertTrue(characters.contains('s'));
        characters.remove((Character) 's');
        assertTrue(characters.contains('t'));
        characters.remove((Character) 't');
        assertTrue(characters.isEmpty());
    }
}
