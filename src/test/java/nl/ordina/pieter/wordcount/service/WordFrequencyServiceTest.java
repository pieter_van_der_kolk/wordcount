package nl.ordina.pieter.wordcount.service;

import nl.ordina.pieter.wordcount.contract.WordFrequency;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class WordFrequencyServiceTest {

    private WordFrequencyService cut = new WordFrequencyService();

    @Test
    void calculateHighestFrequency_should_return_frequency_of_word_that_has_highest_frequency_in_text() {
        String text = "'the' is the word that has the highest frequency in this text and the frequency is 4";
        int frequency = cut.calculateHighestFrequency(text);
        assertEquals(4, frequency);
    }

    @Test
    void calculateHighestFrequency_should_return_frequency_also_if_more_than_one_word_has_highest_frequency() {
        String text = "the the the and is is is both have a frequency of 3";
        int frequency = cut.calculateHighestFrequency(text);
        assertEquals(3, frequency);
    }

    @Test
    void calculateFrequencyForWord_should_return_the_frequency_of_a_given_word() {
        String text = "What is the frequency of the word the in this text? I think the answer is 4.";
        int frequency = cut.calculateFrequencyForWord(text, "the");
        assertEquals(4, frequency);
    }

    @Test
    void calculateFrequencyForWord_should_return_the_frequency_of_a_given_word_independent_of_capitalization() {
        String text = "What is the frequency of the word the in this text? I think the answer is 4.";
        int frequency = cut.calculateFrequencyForWord(text, "FREQUENCY");
        assertEquals(1, frequency);
    }

    @Test
    void calculateMostFrequentNWords_should_return_n_most_frequent_words() {
        String text = "  567    hello;hello,hello!-hello45678hello " + // 5 x hello
                "this  this    this             this    " + // 4 x this
                "is\t\\is\ris\n " + // 3 x is
                "a____________a " + // 2 x a
                "test.,098643++"; // 1 x test
        List<WordFrequency> wordFrequencies = cut.calculateMostFrequentNWords(text, 5);
        assertEquals("hello", wordFrequencies.get(0).getWord());
        assertEquals(5, wordFrequencies.get(0).getFrequency());
        assertEquals("this", wordFrequencies.get(1).getWord());
        assertEquals(4, wordFrequencies.get(1).getFrequency());
        assertEquals("is", wordFrequencies.get(2).getWord());
        assertEquals(3, wordFrequencies.get(2).getFrequency());
        assertEquals("a", wordFrequencies.get(3).getWord());
        assertEquals(2, wordFrequencies.get(3).getFrequency());
        assertEquals("test", wordFrequencies.get(4).getWord());
        assertEquals(1, wordFrequencies.get(4).getFrequency());
    }

    @Test
    void calculateMostFrequentNWords_should_return_n_most_frequent_words_independent_of_capitalization() {
        String text = "this This tHIS THIS is Is iS a A test";
        List<WordFrequency> wordFrequencies = cut.calculateMostFrequentNWords(text, 2);
        assertEquals("this", wordFrequencies.get(0).getWord());
        assertEquals(4, wordFrequencies.get(0).getFrequency());
        assertEquals("is", wordFrequencies.get(1).getWord());
        assertEquals(3, wordFrequencies.get(1).getFrequency());
    }

    @Test
    void calculateMostFrequentNWords_should_return_words_with_same_frequency_in_alphabetical_order() {
        String text = "ac ab aa d d";
        List<WordFrequency> wordFrequencies = cut.calculateMostFrequentNWords(text, 3);
        assertEquals("d", wordFrequencies.get(0).getWord());
        assertEquals(2, wordFrequencies.get(0).getFrequency());
        assertEquals("aa", wordFrequencies.get(1).getWord());
        assertEquals(1, wordFrequencies.get(1).getFrequency());
        assertEquals("ab", wordFrequencies.get(2).getWord());
        assertEquals(1, wordFrequencies.get(2).getFrequency());
    }
}
