package nl.ordina.pieter.wordcount.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WordFrequencyControllerTest {

    @LocalServerPort
    private int port = 8080;
    private String baseUrl = "http://localhost:" + port + "/api";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void getHighestFrequency_should_return_word_with_highest_frequency_from_text() {
        String text = "the word that has the highest frequency in this text is the word the and the " +
                "frequency of the word the is 7";
        String suffix = String.format("/highestfrequency/%s", text);
        assertThat(this.restTemplate.getForObject(baseUrl + suffix, String.class)).isEqualTo("7");
    }

    @Test
    void postCalculateHighestFrequency_should_return_word_with_highest_frequency_from_text() {
        String text = "the word??that has the___highest frequency in this text is the word the and the " +
                "frequency of   the1234556word the is 7";
        String suffix = "/highestfrequency";
        assertThat(this.restTemplate.postForObject(baseUrl + suffix, text, String.class)).isEqualTo("7");
    }

    @Test
    void getFrequencyForWord_should_return_frequency_for_word() {
        String text = "the word that has the highest frequency in this text is the word the and the " +
                "frequency of the word the is 7";
        String word = "the";
        String suffix = String.format("/frequency/%s/%s", word, text);
        assertThat(this.restTemplate.getForObject(baseUrl + suffix, String.class)).isEqualTo("7");
    }

    @Test
    void postCalculateFrequencyForWord_should_return_frequency_for_word() {
        String text = "\tthe____word++that__has;)the12345678highest \\frequency@#in....this     text is the word " +
                "the and^^^&&&*the?|frequency}{of==the word the is 7||||||";
        String word = "the";
        String suffix = String.format("/frequency/%s", word);
        String response = this.restTemplate.postForObject(baseUrl + suffix, text, String.class);
        System.out.println(response);
        assertThat(this.restTemplate.postForObject(baseUrl + suffix, text, String.class)).isEqualTo("7");
    }

    @Test
    void getMostFrequentNWords_should_return_the_n_words_with_highest_frequency() {
        String text = "the word that has the highest frequency in this text is the word the and the " +
                "frequency of the word the is 7";
        String n = "3";
        String suffix = String.format("/highestfrequencies/%s/%s", n, text);
        assertThat(this.restTemplate.getForObject(baseUrl + suffix, String.class)).contains("the", "word", "frequency");
    }

    @Test
    void postCalculateMostFrequentNWords_should_return_the_n_words_with_highest_frequency() {
        String text = "\tthe____word++that__has;)the12345678highest \\frequency@#in....this     text is the word " +
                "the and^^^&&&*the?|frequency}{of==the word the is 7||||||";
        String n = "3";
        String suffix = String.format("/highestfrequencies/%s", n);
        assertThat(this.restTemplate.postForObject(baseUrl + suffix, text, String.class)).contains("the", "word", "frequency");
    }
}
