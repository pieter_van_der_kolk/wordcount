package nl.ordina.pieter.wordcount.service;

import nl.ordina.pieter.wordcount.contract.WordFrequency;
import nl.ordina.pieter.wordcount.contract.WordFrequencyAnalyzer;
import nl.ordina.pieter.wordcount.util.WordFrequencyUtil;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class WordFrequencyService implements WordFrequencyAnalyzer {

    public int calculateHighestFrequency(String text) {
        Map<String, Integer> wordFrequencies = WordFrequencyUtil.createMapOfWordFrequencies(text);
        return Collections.max(wordFrequencies.values());
    }

    public int calculateFrequencyForWord(String text, String word) {
        Map<String, Integer> wordFrequencies = WordFrequencyUtil.createMapOfWordFrequencies(text);
        return wordFrequencies.getOrDefault(word.toLowerCase(), 0);
    }

    public List<WordFrequency> calculateMostFrequentNWords(String text, int n) {
        return WordFrequencyUtil.createListOfTopNWordFrequencies(text, n);
    }
}
