package nl.ordina.pieter.wordcount.controller;

import nl.ordina.pieter.wordcount.service.WordFrequencyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class WordFrequencyController {

    private WordFrequencyService wordFrequencyService;

    public WordFrequencyController(WordFrequencyService wordFrequencyService) {
        this.wordFrequencyService = wordFrequencyService;
    }

    @GetMapping("/highestfrequency/{text}")
    public ResponseEntity<?> getHighestFrequency(@PathVariable(value = "text") String text) {
        return handleHighestFrequencyRequest(text);
    }

    @PostMapping("/highestfrequency")
    public ResponseEntity<?> postCalculateHighestFrequency(@RequestBody String text) {
        return handleHighestFrequencyRequest(text);
    }

    private ResponseEntity<?> handleHighestFrequencyRequest(String text) {
        text = text.replaceAll("\\\\", "_"); // remove escape characters
        if (text.matches(".*[a-zA-Z].*")) {
            return ResponseEntity.ok().body(wordFrequencyService.calculateHighestFrequency(text));
        } else {
            return ResponseEntity.badRequest().body(String.format(
                    "Text should have at least one letter, your text was: %s", text));
        }
    }

    @GetMapping("/frequency/{word}/{text}")
    public ResponseEntity<?> getFrequencyForWord(@PathVariable(value = "word") String word,
                                                 @PathVariable(value = "text") String text) {
        return handleFrequencyForWordRequest(text, word);
    }

    @PostMapping("/frequency/{word}")
    public ResponseEntity<?> postCalculateFrequencyForWord(@PathVariable(value = "word") String word,
                                                           @RequestBody String text) {
        return handleFrequencyForWordRequest(text, word);
    }

    private ResponseEntity<?> handleFrequencyForWordRequest(String text, String word) {
        text = text.replaceAll("\\\\", "_"); // remove escape characters
        boolean textHasLetters = text.matches(".*[a-zA-Z].*");
        boolean wordHasOnlyLetters = !word.matches(".*[^a-zA-Z].*");
        if (textHasLetters && wordHasOnlyLetters) {
            return ResponseEntity.ok().body(wordFrequencyService.calculateFrequencyForWord(text, word));
        } else {
            if (!textHasLetters) {
                return ResponseEntity.badRequest().body(String.format(
                        "Text should have at least one letter, your text was: %s.", text));
            } else { // !wordHasOnlyLetters
                return ResponseEntity.badRequest().body(String.format(
                        "Word should have only letters, your word was: %s.", word));
            }

        }
    }

    @GetMapping("/highestfrequencies/{n}/{text}")
    public ResponseEntity<?> getMostFrequentNWords(@PathVariable(value = "n") int n,
                                                   @PathVariable(value = "text") String text) {
        return handleMostFrequentNWordsRequest(text, n);
    }

    @PostMapping("/highestfrequencies/{n}")
    public ResponseEntity<?> postCalculateMostFrequentNWords(@PathVariable(value = "n") int n,
                                                             @RequestBody String text) {
        return handleMostFrequentNWordsRequest(text, n);
    }

    private ResponseEntity<?> handleMostFrequentNWordsRequest(String text, int n) {
        text = text.replaceAll("\\\\", "_"); // remove escape characters
        boolean nIsGreaterThanZero = n > 0;
        boolean textHasLetters = text.matches(".*[a-zA-Z].*");
        if (nIsGreaterThanZero && textHasLetters) {
            return ResponseEntity.ok().body(wordFrequencyService.calculateMostFrequentNWords(text, n));
        } else {
            if (!textHasLetters) {
                return ResponseEntity.badRequest().body(String.format(
                        "Text parameter should have at least one letter, your text was: %s", text));
            } else { // !nIsGreaterThanZero
                return ResponseEntity.badRequest().body(String.format(
                        "N parameter should be greater than zero, your number was: %s", text));
            }
        }
    }
}
