package nl.ordina.pieter.wordcount.util;

import nl.ordina.pieter.wordcount.contract.WordFrequency;
import nl.ordina.pieter.wordcount.model.WordFrequencyObject;

import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyUtil {

    public static List<WordFrequency> createListOfTopNWordFrequencies(String text, int n) {

        List<WordFrequency> wordFrequencies = new ArrayList<>();
        Map<String, Integer> topNWordFrequencies = getTopNWordFrequencies(text, n);

        topNWordFrequencies.forEach((k, v) -> {
            WordFrequencyObject wfo = new WordFrequencyObject(k, v);
            wordFrequencies.add(wfo);
        });

        return wordFrequencies;
    }

    static Map<String, Integer> getTopNWordFrequencies(String text, int n) {
        Map<String, Integer> wordFrequencyMap = createMapOfWordFrequencies(text);
        return wordFrequencyMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(n)
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

   public static Map<String, Integer> createMapOfWordFrequencies(String text) {

        List<String> words = createListOfWords(text);
        Map<String, Integer> wordFrequencyMap = new TreeMap<>(); // so words are in alphabetical order

        for (String word : words) {
            if (!wordFrequencyMap.containsKey(word))
                wordFrequencyMap.put(word, 1);
            else
                wordFrequencyMap.put(word, wordFrequencyMap.get(word) + 1);
        }

        return wordFrequencyMap;
    }

    static List<String> createListOfWords(String text) {

        List<Character> characters = createListOfCharacters(text);
        StringBuilder word = new StringBuilder();
        List<String> words = new ArrayList<>();
        boolean wordHasStarted = false;
        int textHasEnded = characters.size() - 1;

        for (int i = 0; i < characters.size(); i++) {

            // we are at the beginning or somewhere inside a word
            if (Character.isLetter(characters.get(i))) {
                wordHasStarted = true;
                word.append(characters.get(i));

                // we have reached the end of the text
                if (i == textHasEnded) {
                    words.add(word.toString());
                }
            }

            // we have reached the end of a word
            if (!Character.isLetter(characters.get(i)) && wordHasStarted) {
                wordHasStarted = false;
                words.add(word.toString());
                word.setLength(0);
            }
        }

        return words;
    }

    static List<Character> createListOfCharacters(String text) {
        text = text.toLowerCase(); // we are interested in case insensitive frequency
        return text.chars().mapToObj(e -> (char) e).collect(Collectors.toList());
    }
}
