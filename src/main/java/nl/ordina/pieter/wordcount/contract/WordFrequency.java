package nl.ordina.pieter.wordcount.contract;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}
