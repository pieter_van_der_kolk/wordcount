package nl.ordina.pieter.wordcount.model;

import nl.ordina.pieter.wordcount.contract.WordFrequency;

public class WordFrequencyObject implements WordFrequency {

    private String word;
    private int frequency;

    public WordFrequencyObject(String word, int frequency) {
        this.word = word;
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public int getFrequency() {
        return frequency;
    }

    @Override
    public String toString() {
        return "WordFrequencyObject{" +
                "word='" + word + '\'' +
                ", frequency=" + frequency +
                '}';
    }
}
